
import 'package:flutter/material.dart';
import 'package:week4/validation/vali.dart';



class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login Me",
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            passwordField(),
            loginButton(),
          ],
        )
      )
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: "Email address"),
      validator: validateEmail,
      onSaved: (value){
        email = value as String;
      },
    );
  }

  Widget passwordField(){
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: "Password"),
      validator: validatePassword,
      onSaved: (value){
        print('onSaved: value=$value');
      },
    );
  }

  Widget loginButton(){
    return ElevatedButton(
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();
        }
      }, 
      child: Text('Login'));
  }

}