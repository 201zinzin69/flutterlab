mixin CommonValidation{
  String? validateEmail(String? value){
    if(!(value!.contains('@') && value.contains('.'))){
      return "Invalid email";
    }
    else{
      return null;
    }
  }

  String? validatePassword(String? value){
    if(value!.length < 4){
      return "Invalid password";
    }
    else{
      return null;
    }
  }
}