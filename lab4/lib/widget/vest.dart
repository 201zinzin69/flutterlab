import 'package:flutter/material.dart';

class CardWidget2 extends StatelessWidget {  
  // This widget is the root of your application.  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: MyGridScreen(),  
    );  
  }  
}  
  
class MyGridScreen extends StatefulWidget {   
  
  @override  
  _MyGridScreenState createState() => _MyGridScreenState();  
}  
  
class _MyGridScreenState extends State<MyGridScreen> {  
  @override  
  Widget build(BuildContext context) {  
    return Scaffold(    
      body: Center(  
          child: GridView.extent(  
            primary: false,  
            padding: const EdgeInsets.all(16),  
            crossAxisSpacing: 100,  
            mainAxisSpacing: 10,  
            maxCrossAxisExtent: 200.0,  
            children: <Widget>[  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest4.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest1.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest2.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest3.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest5.jpg')),  
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest4.jpg')),    
              ), 
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest7.jpg')),    
              ),  
              Container(  
                padding: const EdgeInsets.all(8),  
                child: Image(image: AssetImage('vest8.jpg')),    
              ), 
            ],  
          )),  
    );  
  }  
}  