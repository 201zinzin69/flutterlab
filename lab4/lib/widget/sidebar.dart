import 'package:flutter/material.dart';

class SidebarWidget extends StatelessWidget {
  const SidebarWidget({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return     new Drawer(
        child: ListView(children: [
          new UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              child: Image.asset('conmeo.png')
            ),
              accountName: Text('Trần Hoài Phát',style: TextStyle(fontSize: 20),),
              accountEmail: Text('519h0332@gmai.com'))
        ,ListTile(
          title: Text('Home'),
          onTap: (){},
        )
        ,ListTile(
          title: Text('Voucher'),
          onTap: (){},
        )
        ,ListTile(
          title: Text('Contact us'),
          onTap: (){},
        )
        ]),
      );


  }
}