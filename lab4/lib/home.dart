import 'package:flutter/material.dart';
import 'package:lab4/widget/Pants.dart';
import 'package:lab4/widget/Shirt.dart';
import 'package:lab4/widget/sidebar.dart';
import 'package:lab4/widget/vest.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {

late TabController _tabController;

@override
  void initState() {
    _tabController = TabController(length: 3, vsync: this,initialIndex: 0)..addListener(() {
setState(() {
  
});      
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shadowColor: Colors.transparent,
        title: Text('Welcome'),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
          IconButton(icon: Icon(Icons.shopping_bag_outlined), onPressed: () {}),
        ],
        bottom: TabBar(
          controller: _tabController,
          labelStyle: TextStyle(fontSize: 15),
          indicatorSize: TabBarIndicatorSize.label,
          labelPadding: EdgeInsets.symmetric(horizontal: 60, vertical: 20),
          isScrollable: true,
          tabs: [
            Text('T-Shirt'),
            Text('Pants'),
            Text('Vest'),

            

        ],),
      ),
      drawer: SidebarWidget(),
      body: TabBarView(
        controller: _tabController,
        children: [
CardWidget(),
CardWidget1(),
CardWidget2(),

        ],),
    );
  }
}
