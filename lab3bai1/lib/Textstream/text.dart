import 'package:flutter/material.dart';


class Textstream{
  Stream<String> getMessage() async*{
    final List<String> strings = 
    [
      'Hello',
      'My Name is Phat',
      'And you?',
      'My nam is Cuong',
    ];
    yield* Stream.periodic(Duration(seconds: 5),(int t){
      int index = t % 7;
      return strings[index];
    });
  }
}